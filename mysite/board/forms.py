from django.forms import ModelForm, Textarea
from .models import Company, Project, CompanyComment
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User

class CompanyForm(ModelForm):
    class Meta:
        model= Company
        fields = ('name', 'fio', 'content', 'done', 'adress', 'phone', 'email' )
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class']='form-control'
class ProjectForm(ModelForm):
    class Meta:
        model= Project
        fields = ('title', 'content', 'date1', 'date2', 'price' )
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class']='form-control'
class AuthForm(AuthenticationForm, ModelForm):
    class Meta:
        model= User
        fields = ('username', 'password')
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class']='form-control'

class RegisterForm(ModelForm):
    class Meta:
        model= User
        fields = ('first_name', 'last_name', 'username', 'email', 'password')
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class']='form-control'
    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

class RegisterFormm(ModelForm):
    class Meta:
        model= User
        fields = ('first_name', 'last_name', 'username', 'email')
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class']='form-control'
    def save(self, commit=True):
        user = super().save(commit=False)
        if commit:
            user.save()
        return user


class CommentForm(ModelForm):
    class Meta:
        model= CompanyComment
        fields = ('manager', 'text', 'chanel', 'ergebnis')
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class']='form-control'

