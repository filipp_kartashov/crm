from django.db import models
from phone_field import PhoneField
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
Account = get_user_model()


class Company(models.Model):
    author = models.ForeignKey(User, on_delete = models.CASCADE, verbose_name="Автор", null=True,
        blank=True)

    name = models.CharField(
        max_length=20,
        db_index=True,
        verbose_name="Название компании")


    fio = models.CharField(
        max_length=50,
        db_index=True,
        verbose_name="ФИО директора")
    content = models.TextField(
        null=True,
        blank=True,
        verbose_name="Описание компании")
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True,
        verbose_name="Дата публикации")
    done = models.DateTimeField(
        auto_now_add=False,
        db_index=True,
        verbose_name="Дата")
    adress = models.CharField(
        max_length=50,
        db_index=True,
        verbose_name="Адрес")
    phone = PhoneField(
        null=True,
        blank=True,
        verbose_name="Телефон")
    email = models.EmailField(
        null=True,
        blank=True,
        verbose_name="Почта")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural='Компании'
        verbose_name = 'Компания'
        ordering = ['-published']

class Project(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Автор", null=True,
                               blank=True)
    company = models.ForeignKey(
        'Company',
        null=True,
        on_delete=models.PROTECT,
        verbose_name='Компания')

    title = models.CharField(
        max_length=20,
        db_index=True,
        verbose_name="Название проекта")


    content = models.TextField(
        null=True,
        blank=True,
        verbose_name="Описание проекта")
    date1 = models.DateTimeField(
        auto_now_add=False,
        db_index=True,
        verbose_name="Дата начала")
    date2 = models.DateTimeField(
        auto_now_add=False,
        db_index=True,
        verbose_name="Дата завершения")
    price = models.FloatField(
        null=True,
        blank=True,
        verbose_name="Цена")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural='Проекты'
        verbose_name = 'Проект'
        ordering = ['-date1']

class CompanyComment(models.Model):
    company = models.ForeignKey(
        'Company',
        null=True,
        on_delete=models.CASCADE,
        verbose_name='Компания')


    project = models.ForeignKey(
        'Project',
        null=True,
        on_delete=models.CASCADE,
        verbose_name='Проект')


    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Автор комментария", null=True,
                               blank=True)

    manager = models.CharField(
        max_length=50,
        db_index=True,
        verbose_name="Менеджер"
    )
    text = models.TextField(
        null=True,
        blank=True,
        verbose_name="Описание")
    CHOICES = (
        ("Заявка", "Заявка"),
        ("Письмо", "Письмо"),
        ("Сайт", "Сайт"),
        ("Входящее письмо", "Входящее письмо"),
        ("Инициатива компании", "Инициатива компании"),
    )
    chanel = models.CharField(max_length=300, choices=CHOICES, verbose_name="Канал общения" )
    CHOICE = (
        ("Like", "Like"),
        ("Dislike", "Dislike"),
    )
    ergebnis = models.CharField(max_length=300, choices=CHOICE, verbose_name="Оценка")
    status = models.BooleanField(verbose_name="Статус", default=False)
    class Meta:
        verbose_name_plural='Комментарии'
        verbose_name = 'Комментарий'
        ordering = ['-company']




        


